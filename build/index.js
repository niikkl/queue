"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Queue = void 0;
const QueueEmptyException_1 = require("./Exceptions/QueueEmptyException");
class Queue {
    constructor(seed, unique = true) {
        this.queue = [seed];
        this.unique = unique;
    }
    start(process) {
        this.shift().then(item => {
            if (process(item) === undefined || process(item) === true) {
                this.start(process);
            }
        }).catch(() => undefined);
    }
    push(...items) {
        let filtered = [];
        if (this.unique) {
            items.forEach((item) => {
                if (!this.queue.includes(item)) {
                    filtered.push(item);
                }
            });
        }
        else {
            filtered = items;
        }
        this.queue.push(...filtered);
    }
    shift() {
        const item = this.queue.shift();
        if (!item) {
            return Promise.reject(new QueueEmptyException_1.QueueEmptyException('Queue empty'));
        }
        return Promise.resolve(item);
    }
}
exports.Queue = Queue;
//# sourceMappingURL=index.js.map