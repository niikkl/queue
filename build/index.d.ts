export declare class Queue<Item> {
    private queue;
    private readonly unique;
    constructor(seed: Item, unique?: boolean);
    start(process: (item: Item) => void | boolean): void;
    push(...items: Item[]): void;
    private shift;
}
//# sourceMappingURL=index.d.ts.map