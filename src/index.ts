import {QueueEmptyException} from './Exceptions/QueueEmptyException';

export class Queue<Item> {
    private queue: Item[];
    private readonly unique: boolean;

    public constructor(seed: Item, unique = true) {
        this.queue = [seed];
        this.unique = unique;
    }

    public start(process: (item: Item) => void | boolean): void {
        this.shift().then(item => {
            if (process(item) === undefined || process(item) === true) {
                this.start(process);
            }
        }).catch(() => {
            setTimeout(() => this.start(process), 5000);
        });
    }

    public push(...items: Item[]): void {
        let filtered: Item[] = [];

        if (this.unique) {
            items.forEach((item) => {
                if (!this.queue.includes(item)) {
                    filtered.push(item);
                }
            });
        } else {
            filtered = items;
        }

        this.queue.push(...filtered);
    }

    private shift(): Promise<Item> {
        const item: Item | undefined = this.queue.shift();

        if (!item) {
            return Promise.reject(
                new QueueEmptyException('Queue empty')
            );
        }
        return Promise.resolve(item);
    }
}